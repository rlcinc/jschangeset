# README #

You can use line this to create a changeset with title:
**id,id,id[:title]**

Please put this code as bookmark for **create a changeset** from a id's list

```
#!javascript

javascript: (function() {
	function init() {
		jQuery.get("https://bitbucket.org/rlcinc/jschangeset/raw/master/js/changeSet.js", function(data) {
			eval(data);
		});
	}
	_my_script = document.createElement('SCRIPT');
	_my_script.type = 'text/javascript';
	_my_script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js';
	_my_script.onload = init;
	document.getElementsByTagName('head')[0].appendChild(_my_script);
})();
```

If you need **add** current page to list, please use this bookmark 

```
#!javascript

javascript: (function() {
    function init() {
        jQuery.get("https://bitbucket.org/rlcinc/jschangeset/raw/master/js/prepareSet.js", function(data) {
            eval(data);
	    initLst('add');
        });
    }
	if (!($ = window.jQuery)) {
		script = document.createElement('script');
		script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js';
		script.onload = init;
		document.body.appendChild(script);
	}
	else {
		init();
	}
})();
```

If you need **delete** list of items, please use this bookmark 

```
#!javascript

javascript: (function() {
    function init() {
        jQuery.get("https://bitbucket.org/rlcinc/jschangeset/raw/master/js/prepareSet.js", function(data) {
            eval(data);
	    initLst('del');
        });
    }
	if (!($ = window.jQuery)) {
		script = document.createElement('script');
		script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js';
		script.onload = init;
		document.body.appendChild(script);
	}
	else {
		init();
	}
})();
```

If you need **show** current list, please use this bookmark 

```
#!javascript

javascript: (function() {
    function init() {
        jQuery.get("https://bitbucket.org/rlcinc/jschangeset/raw/master/js/prepareSet.js", function(data) {
            eval(data);
	    initLst('show');
        });
    }
	if (!($ = window.jQuery)) {
		script = document.createElement('script');
		script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js';
		script.onload = init;
		document.body.appendChild(script);
	}
	else {
		init();
	}
})();
```