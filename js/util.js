/*

Copyright by Dmitry Rivlin email: wildervit@gmail.com

*/
    function listMetadataWithoutWildCards() {
        /*Create a list of unsupported wildcard metadata. Please see "coreConfig.cls" variable : "withOutWildMetaData"*/
        var mData = [];
        $('.sort_table tr').each(

            function() {

                var tdLst = $(this).find('td');
                for (var i = 0; i < tdLst.length; i++) {
                    if ($(tdLst[i]).attr('data-title') == 'Allows Wildcard (*)?' && tdLst[i].innerText.toLowerCase() != 'yes') {
                        var mType = tdLst[i - 1].innerText.replace(/\(.*\)/g, '').trim();
                        console.log(mType + ' : ' + tdLst[i].innerText);
                        mData.push(mType);
                    }
                }
            });
        var result = "	public static final set<string> withOutWildMetaData = new set<string>{'" + mData.join("', '") + "'};";
        $('#topic-title').html('<pre style="font-size: 10pt;">' + result.replace(/</g,'&lt;') + '</pre>');
        console.log(result);
    }

function extractSQL() {
var o = document.evaluate("//div[@id='editors-body']/div[not(contains(@style,'display:none') or contains(@style,'display: none'))]//table/tbody/tr",document,null,0,null);
var r = [];
while(row = o.iterateNext()){
    var cols = row.getElementsByTagName('td');
    var a = [];

    for(var i=0; i<cols.length; i++){
        a.push( formatData( cols[i].textContent ) );
    }

    r.push( a );
}

// generating csv file
var csv = "data:text/csv;charset=utf-8,filename=download.csv,";
var rows = [];

for(var i=0; i<r.length; i++){
    rows.push( r[i].join(",") );
}

csv += rows.join('\r\n');

popup(csv);
}

function formatData(input) {
    // replace " with �
    var regexp = new RegExp(/["]/g);
    var output = input.replace(regexp, "�");
    //HTML
    var regexp = new RegExp(/\<[^\<]+\>/g);
    var output = output.replace(regexp, "");
    if (output == "") return '';
    return '"' + output + '"';
}

// showing data in window for copy/ paste
function popup(data) {
    var generator = window.open('', 'csv', 'height=400,width=600');
    generator.document.write('<html><head><title>CSV</title>');
    generator.document.write('</head><body style="overflow: hidden;">');
    generator.document.write('<a href="'+encodeURI(data)+'" download="Sf_export.csv">Download CSV</a><br>');
    generator.document.write('<textArea style="width: 100%; height: 97%;" wrap="off" >');
    generator.document.write(data);
    generator.document.write('</textArea>');
    generator.document.write('</body></html>');
    generator.document.close();
    return true;
}

	saveFile = function(response, fName) {

		function errorHandler(e) {
			console.log(e);
		}

		function onInitFs(fs) {
			fs.root.getFile(fName, {
				create: true,
				exclusive: false
			}, function(fileEntry) {
				var truncated = false;
				fileEntry.createWriter(function(fileWriter) {
					fileWriter.onwrite = function(evt) {
						console.log('WRITE ' + evt);
					}
					fileWriter.onwriteend = function(e) {
						if (!truncated) {
							truncated = true;
							this.truncate(this.position);
							return;
						}

						console.log(fileEntry);

						var downloadLink = document.createElement("a");
						downloadLink.href = fileEntry.toURL();
						//console.log(downloadLink.href);
						downloadLink.download = fileEntry.name;

						document.body.appendChild(downloadLink);
						downloadLink.click();
						document.body.removeChild(downloadLink);
					};
					fileWriter.onerror = function(e) {
						console.log('Write failed: ' + e.toString());
					};
					var blob = new Blob([response], {
						type : 'text/plain'
					}); //application/octet-stream
					console.log(blob);
					console.log(response);
					fileWriter.write(blob);
				}, errorHandler);
			}, errorHandler);
		}
		window.requestFileSystem = window.requestFileSystem || window.webkitRequestFileSystem;
		window.requestFileSystem(window.TEMPORARY, 1024, onInitFs, errorHandler);
	}



log = '';
function getDeployments(inTable) {
    var id = inTable== true 
	?'ListInboundChangeSetPage\\:listInboundChangeSetPageBody\\:listInboundChangeSetPageBody\\:ListInboundChangeSetForm\\:DeployedPageBlock\\:ListDeployedInboundChangeSetBlockSection\\:DeployedInboundChangeSetList .dataRow' 
	: 'MonitorDeploymentsPage\\:deploymentsForm\\:listSucceeded\\:SucceededDeploymentsList .dataRow';
    jQuery('#' + id).each(function(a, b) {
        var cnt = 0;
        var row = {};
        jQuery(this).find('td').each(function(a, b) {
            //if (cnt != 2) {
                var context = jQuery(this).text().trim();
                row[cnt] = context;
                log += context + '\t';
            //}
            cnt++;
        });
        log += '\n';
        //console.log(row);  
    });

    if (jQuery('[id$="nextPageLink"]').length != 0) {
        jQuery('[id$="nextPageLink"]').click();
        setTimeout(function() {
            getDeployments(inTable)
        }, 20000);
    } else {
	saveFile(log,'report.txt');
      console.log(log);
    }
console.log('COUNTER :' + counter);
}