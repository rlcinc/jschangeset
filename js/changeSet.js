/*

Copyright by Dmitry Rivlin email: wildervit@gmail.com

*/
  chSetImpl = function() {
	var that = this;
	this.idlst = '';
  
	this.firstInit = function() {
	   that.createChSet();
	}
	
	this.createChSet = function() {
		if (location.href.indexOf('/changemgmt/inboundChangeSetDetailPage.apexp')!=-1 || location.href.indexOf('/changemgmt/outboundChangeSetDetailPage.apexp')!=-1) {
			that.preparePackageXml();
			return;
		}
		var lst = (typeof(window.getList) !='undefined') ? window.getList().replace(/,$/,'') : '';
		that.idlst = prompt("Please put list of Ids divided by ','",lst);
		if (!that.idlst) return;
		jQuery("body").append('<div id="ssOverlay" style="background: url(https://www.lightningdesignsystem.com/assets/images/spinners/slds_spinner.gif) center center no-repeat; width: 100%; height: 100%; position: fixed; top: 0px; left: 0px; z-index: 1000; vertical-align: middle; font-weight: bold; font-size: 12pt; color: red; line-height: 2em; text-align: center; opacity: 0.75; background-color: #ffffff;display:table"><div style="display: table-cell; vertical-align: middle;">Create a changeSet</div></div>');
		var chName = that.idlst.replace(/.*\:(.*$)/,'$1');
		chName = (chName == that.idlst) ? 'Auto Created ChangeSet' : chName;
		console.log('chName ' + chName);
		that.idlst = that.idlst.replace(/(\:.*$)/,'');

		that.idlst = that.idlst.split(/,\s*/);
		var createChaneSetUrl = '/changemgmt/createOutboundChangeSet.apexp';
	
		var ifr=$('<iframe/>', {
			id: 'MainPopupIframe',
			src: createChaneSetUrl ,
			style:'display:none',
			load:function(){
				var curDate = JSON.stringify(new Date());
				var chSetIframe = $("#MainPopupIframe").contents();
				var nameInput = chSetIframe.find("input[id$='changeSetName']");
				var descrInput = chSetIframe.find("textarea[id$='changeSetDescription']");
				var saveBtn = chSetIframe.find("input[id$='saveChangeSet']")[1];
				jQuery(nameInput).val(chName + ' ' + curDate.replace(/.*(\d{4}-\d{2}-\d{2})T(\d{2}:\d{2}:\d{2}).*/,'$1 $2'));
				jQuery(descrInput).val('Created by Dmitry Rivlin automated process. email: wildervit@gmail.com');
				jQuery(saveBtn).click();
				var loc = document.getElementById("MainPopupIframe").contentWindow.location.href;
				loc = loc.replace(/.*\?id=(.*)/,'$1');
				if (loc.length == 15) {
					var idBtn = chSetIframe.find("input[id$='outboundCs_add']");
					jQuery(idBtn).click();
					setTimeout(function(){that.createChSetContent(loc);}, 5000);
				}
			}
		});
		$('body').append(ifr);
	}
	
	this.createChSetContent = function(chId) {
		var uId = document.getElementById("MainPopupIframe").contentWindow.location.href;
		uId = uId.replace(/.*id=(.*)/,'$1');
		var cToken = $("#MainPopupIframe").contents().find("#_CONFIRMATIONTOKEN").val();
		console.log('starting create a change set contents ' + chId + ' ' + uId + ' ' + cToken);
			var body = [];
			body.push("_CONFIRMATIONTOKEN=" + encodeURIComponent(cToken));
			body.push("entityType=" + encodeURIComponent("TabSet"));
			body.push("id=" + encodeURIComponent(uId));
			body.push("save=" + encodeURIComponent("Add To Change Set"));
			for(var i=0; i<that.idlst.length; i++){
				body.push("ids=" + encodeURIComponent(that.idlst[i]));
			}
			body = body.join('&');
			console.log(body);
			jQuery.ajax({
				url: '/p/mfpkg/AddToPackageFromChangeMgmtUi?id=' + uId,
				type: "POST",
				data: body,
				headers: {
					"Content-Type": "application/x-www-form-urlencoded"
				},error: function(error) {
					console.log('Exception in WS : ' + JSON.stringify(error));
				},
				success: function(data, textStatus, jqXHR) {
					//console.log(data);
					window.location.href = '/changemgmt/outboundChangeSetDetailPage.apexp?id=' + chId;
				}
			});
	}
	
	this.preparePackageXml = function() {
		if (typeof(window.packageXml) == 'undefined') window.packageXml = {};
		var t= jQuery('td[id$="type"]').each(function(){
			var tp = jQuery(this).text();

			if (!(tp in window.packageXml)) window.packageXml[tp] = [];
			window.packageXml[tp].push(jQuery(this).next().text());
		});
		that.buildPackageXml();
		console.log(window.packageXml);
		if (jQuery('[id$="nextPageLink"]').length!=0) {
			jQuery('[id$="nextPageLink"]').click();
			setTimeout(function(){that.preparePackageXml()}, 5000);
		}
	}

	this.buildPackageXml = function() {
		var pXml = '<?xml version="1.0" encoding="UTF-8"?>\n<Package xmlns="http://soap.sforce.com/2006/04/metadata">\n';
		for (var key in window.packageXml) {
			pXml += '\t<types>\n';
			for (var i=0;i<window.packageXml[key].length; i++) {
				pXml += '\t\t<members>' + window.packageXml[key][i] + '</members>\n';
			}
			pXml += '\t\t<name>' + key.replace(/ +/g,'').replace('VisualforcePage','ApexPage') +'</name>\n';
			pXml += '\t</types>\n';

		}
		pXml += '\t<version>34.0</version>\n</Package>';
		console.log(pXml);
		jQuery('span[id$="_desc"]').html('<pre style="font-size: 120%;">' + pXml.replace(/</g,'&lt;')+ '</pre>');
	}
	
	var init = function() {
		if (!($ = window.jQuery)) { // typeof jQuery=='undefined' works too
			script = document.createElement('script');
			script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js';
			script.onload = that.firstInit;
			document.body.appendChild(script);
		}
		else {
			that.firstInit();
		}
	}
	init();
}

if (typeof(window.chSet) == 'undefined') window.chSet = new chSetImpl();
else window.chSet.createChSet();
