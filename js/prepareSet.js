/*

Copyright by Dmitry Rivlin email: wildervit@gmail.com

*/
window.lstName = 'RLC_chSet';

window.getList = function(){
	var lst = localStorage.getItem(lstName);
	lst = (lst == null) ? '' : lst +',';
	return lst;
}

function setLst() {
	var id = location.href.replace(/.*\/(.*)/,'$1');
	if (getList().indexOf(id)!=-1) {
		alert('Already in List');
		return;
	}
	localStorage.setItem(lstName, getList() + id);
	console.log('Added Item ' + id);
	alert('Added Item ' + id);
}

function delList() {
	localStorage.removeItem(lstName);
	alert('Deleted all items from list');
}

function initLst(param){
	switch(param) {
	    case 'add':
        	setLst();
	        break;
	    case 'del':
        	delList();
	        break;
	    case 'show':
        	alert(getList().replace(/,/g,"\n"));
	        break;
	}
}