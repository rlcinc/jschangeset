javascript: (function() {
	var url = "https://developer.salesforce.com/docs/atlas.en-us.api_meta.meta/api_meta/meta_types_list.htm";
	if (location.href != url) {
		location.href = url;
		return;
	}
    function init() {
        jQuery.get("https://bitbucket.org/rlcinc/jschangeset/raw/master/js/util.js", function(data) {
            eval(data);
	    listMetadataWithoutWildCards();
        });
    }
    _my_script = document.createElement('SCRIPT');
    _my_script.type = 'text/javascript';
    _my_script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js';
    _my_script.onload = init;
    document.getElementsByTagName('head')[0].appendChild(_my_script);
})();