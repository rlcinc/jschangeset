﻿javascript: (function() {
    function init() {
        jQuery('[id^="display"]').prop('checked', true);
        jQuery('[id^="edit"]').prop('checked', true);
    }
	if (!($ = window.jQuery)) {
		script = document.createElement('script');
		script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js';
		script.onload = init;
		document.body.appendChild(script);
	}
	else {
		init();
	}
})();